﻿#include <iostream>

int main()
{
    std::cout << "Hello World!\n";
    std::string name = "Danila";
    std::cout << name << " " << name.length() << " " << name.front() << " " << name.back();
}
